from setuptools import setup

def readme():
    with open('README.md') as f:
        return f.read()

setup(name='pynx_wrapper',
      version='0.1',
      description='Wrapper for phase retrieval library PYNX',
      long_description=readme(),
      long_description_content_type="text/markdown",
      url='https://gitlab.com/spi_xfel/pynx_wrapper',
      author='Sergey Bobkov',
      author_email='s.bobkov@grid.kiae.ru',
      license='MIT',
      python_requires='>=3.6',
      install_requires=['numpy',
                        'scipy',
                        'matplotlib',
                        'h5py'],
      packages=['pynx_wrapper'],
      scripts=['scripts/pynx_create.py',
               'scripts/pynx_mean_result.py'],
      include_package_data=True,
      zip_safe=False)
