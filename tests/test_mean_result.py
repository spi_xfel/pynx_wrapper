#!/usr/bin/env python

"""
Tests for mean_result.py
Author: Sergey Bobkov
"""

import numpy as np

import pynx_wrapper

def test_cxi(tmpdir):
    filename = tmpdir.join('tmpfile.cxi')
    data = np.random.ranf((100,100,100))
    pynx_wrapper.save_cxi_data(filename, data)

    assert (data == pynx_wrapper.read_cxi_data(filename)).all()

def test_add_phase():
    data = np.random.ranf((100, 100, 100)).astype(np.complex)
    result = pynx_wrapper.add_phase(data, np.pi)

    assert np.isclose(data, result*-1).all()

def test_correct_phase():
    data = np.random.ranf((100, 100, 100)).astype(np.complex)
    data_phased = pynx_wrapper.add_phase(data, np.random.ranf())
    result = pynx_wrapper.correct_phase(data_phased)
    assert np.isclose(data, result).all()

def test_align():
    data = np.zeros((100, 100, 100)).astype(np.complex)
    data[40:61, 40:61, 40:61] = 1
    data_shifted = np.roll(data, np.random.randint(20)-10, 0)
    data_shifted = np.roll(data_shifted, np.random.randint(20)-10, 1)
    data_shifted = np.roll(data_shifted, np.random.randint(20)-10, 2)
    result = pynx_wrapper.align_center_of_mass(data_shifted)
    assert (data == result).all()

def test_recip():
    data = np.random.ranf((100, 100, 100)) + np.random.ranf((100, 100, 100)) * 1j
    result = pynx_wrapper.recip_to_real(pynx_wrapper.real_to_recip(data))
    assert np.isclose(data, result).all()

def test_mean():
    data = np.zeros((100, 100, 100)).astype(np.complex)
    data[40:61, 40:61, 40:61] = 1
    data *= np.random.ranf((100, 100, 100)) + np.random.ranf((100, 100, 100)) * 1j

    data = pynx_wrapper.correct_phase(data)
    data = pynx_wrapper.align_center_of_mass(data)

    data_stack = np.stack([data]*5)

    res_mean = pynx_wrapper.mean_real_data(data_stack)
    # res_fundamental_mode = mean_result.fundamental_mode_data(np.real(data_stack), variance_part=1)
    res_mean_recip = pynx_wrapper.mean_recip_data(data_stack)

    assert np.isclose(res_mean, data).all()
    # assert np.isclose(res_fundamental_mode, np.real(data)).all()
    assert np.isclose(res_mean_recip, data).all()
