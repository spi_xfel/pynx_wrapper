#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Code for averaging of phase data
Author: Sergey Bobkov
"""

import os
import sys
import argparse
import numpy as np
from scipy.ndimage.measurements import center_of_mass as sp_center_of_mass
import h5py


def read_cxi_data(filename):
    """Read structure dataset from cxi file

    Keyword arguments:
    filename -- name of cxi file

    Return:
    data -- numpy array
    """
    with h5py.File(filename, 'r') as h5file:
        return h5file['entry_1/data_1/data'][:]

def save_cxi_data(filename, data):
    """Create CXI file with input structure data

    Keyword arguments:
    filename -- name of cxi file
    data -- numpy array
    """
    with h5py.File(filename, 'w') as h5file:
        h5file['entry_1/image_1/data'] = data
        h5file['entry_1/data_1'] = h5py.SoftLink('/entry_1/image_1')

def correct_phase(data):
    """Uniform add phase to complex real space structure to maximise real part

    Keyword arguments:
    data -- complex numpy array

    Return:
    corr_data -- input data uniformly shifted phase
    """
    squared_sum = np.sum(data)
    fi_0 = -1*np.arctan2(np.imag(squared_sum), np.real(squared_sum))
    return add_phase(data, fi_0)

def add_phase(data, phase):
    """Uniform add phase to complex data

    Keyword arguments:
    data -- complex numpy array
    phase -- phase value (Rad)

    Return:
    corr_data -- input data with added phase shift
    """
    amplitudes = np.abs(data)
    phases = np.angle(data) + phase
    return amplitudes * np.exp(1j*phases)

def align_center_of_mass(data):
    """Shift real data to align center of mass with center of array

    Keyword arguments:
    data -- complex numpy array

    Return:
    roll_data -- input data rolled to align center
    """
    center_of_mass = np.round(sp_center_of_mass(np.abs(data))).astype(int)
    for axis in range(3):
        shift = center_of_mass[axis] - data.shape[axis] // 2
        if shift != 0:
            data = np.roll(data, -shift, axis)

    return data

def mean_real_data(data_stack):
    """Compute mean structure by averaging in real space

    Keyword arguments:
    data_stack -- 4D stack of input real complex structures

    Return:
    mean_data -- mean structure
    """
    for i, data in enumerate(data_stack):
        data = correct_phase(data)
        data = align_center_of_mass(data)
        data_stack[i] = data

    return np.mean(data_stack, axis=0)

def real_to_recip(real_data):
    """ Convert data from real to reciprocal space
    """
    return np.fft.fftshift(np.fft.fftn(np.fft.ifftshift(real_data)))

def recip_to_real(recip_data):
    """ Convert data from reciprocal to real space
    """
    return np.fft.fftshift(np.fft.ifftn(np.fft.ifftshift(recip_data)))

def mean_recip_data(data_stack):
    """Compute mean structure by averaging in reciprocal space

    Keyword arguments:
    data_stack -- 4D stack of input real complex structures

    Return:
    mean_data -- mean structure
    """
    recip_data_stack = np.zeros_like(data_stack)
    for i, data in enumerate(data_stack):
        data = correct_phase(data)
        recip_data_stack[i] = real_to_recip(data)

    return recip_to_real(recip_data_stack.mean(axis=0))

def read_params(filename):
    """ Read python-like PYNX spec into dictionary

    Keyword arguments:
    filename -- file with spec

    Return:
    dict -- dictionary with extracted data
    """
    params = {}
    with open(filename, 'r') as fil:
        lines = fil.readlines()

    for line in lines:
        i = line.find('#')
        if i >= 0:
            line = line[:i]
        if len(line.strip()) < 4:
            continue
        if line.strip()[0] == '#':
            continue
        sep = line.find('=')
        if sep > 0 and sep < (len(line) - 1):
            key = line[:sep].lower().strip()
            val = line[sep + 1:].strip()
            val = val.replace("'", "")
            val = val.replace('"', '')
            params[key] = val
        else:
            print("WARNING: argument not interpreted: %s" % (line))

    return params

def write_params(filename, data):
    """ Write dictionary into python-like PYNX spec

    Keyword arguments:
    filename -- file to save spec
    data -- dictionary with spec data
    """
    with open(filename, 'w') as fil:
        for key, val in data.items():
            fil.write("{} = {}\n".format(key, val))

def read_default_params():
    """ Read default PYNX spec

    Return:
    dict -- dictionary with default data
    """
    default_file = os.path.join(os.path.dirname(__file__), 'default.txt')
    return read_params(default_file)

def predict_support_size(target_size, wavelength, pixel_size, det_distance, filename):
    """ Predict support size by size of target object

    Keyword arguments:
    target_size -- size of target object in Angstrom
    wavelength -- experiment wavelenght (Angstrom)
    pixel_size -- experiment pixel size (m)
    det_distance -- experiment distance to detector (m)
    filename -- path to cxi file

    Return:
    dict -- dictionary with default data
    """
    with h5py.File(filename, 'r') as h5file:
        shape = h5file['entry_1/data_1/data'].shape

    corner_pix_radius = min(shape) // 2

    d_corner = 2*corner_pix_radius*pixel_size
    z_corner = np.sqrt(det_distance**2 + d_corner**2)
    real_pixel_size = wavelength*z_corner/d_corner

    return target_size/real_pixel_size
