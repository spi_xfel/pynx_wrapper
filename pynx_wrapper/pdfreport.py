"""
Library for PDF report
Author: Sergey Bobkov
"""

import datetime
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.colors as colors
from mpl_toolkits.axes_grid1 import make_axes_locatable

mpl.rcParams['image.cmap'] = 'viridis'

from pynx_wrapper import correct_phase


class ReportWriter():
    """Produce PDF reports with tables and matplotlib figures"""
    def __init__(self, filename: str):
        self._create_file(filename)

    def _create_file(self, filename: str):
        self.pdf_pages = PdfPages(filename)
        pdf_dict = self.pdf_pages.infodict()
        pdf_dict['CreationDate'] = datetime.datetime.today()
        pdf_dict['ModDate'] = datetime.datetime.today()

    def save_figure(self, figure: plt.Figure):
        """Add a page to PDF with matplotlib figure

        Keyword arguments:
        figure -- figure to be saved
        """

        self.pdf_pages.savefig(figure)
        plt.close(figure)

    def close(self):
        """Closes PDF document"""
        self.pdf_pages.close()


def plot_image(image, axis, logscale = True, colorbar = True, vmin = None, vmax = None, **kwargs):
    """Plot image to axis with optional logscale and colorbar

    Keyword arguments:
    image -- 2D array with data to show
    axis -- matplotlib axis to plot
    logscale -- use logscale
    colorbar -- add colorbar to image
    vmin -- minimal value of colormap
    vmax -- miximum value of colormap
    kwargs -- arguments for imshow
    """
    if vmin is None and logscale:
        vmin = max(1, np.amin(image))
    elif vmin is None:
        vmin = np.amin(image)

    if vmax is None:
        vmax = np.amax(image)

    if logscale:
        norm = colors.LogNorm(vmin=vmin, vmax=vmax)
    else:
        norm = colors.Normalize(vmin=vmin, vmax=vmax)

    im_handle = axis.imshow(image, norm=norm, **kwargs)
    axis.set_xticks([])
    axis.set_yticks([])

    if colorbar:
        divider = make_axes_locatable(axis)
        cb_axis = divider.append_axes("right", size="5%", pad=0.05)
        plt.colorbar(im_handle, cax=cb_axis)


def cut_center(data, margin=0, threshold=0.01):
    """ Cut middle area in 3D volume, detect size automatically

    Keyword arguments:
    data -- 3D real data
    margin -- additional margin around selected data

    Return:
    sel_data -- selected subset of data
    """

    ndim = data.ndim

    if ndim not in [1, 2, 3]:
        raise ValueError('Data have unsupported number of dimensions,' + \
                         '{} not in [1, 2, 3]'.format(ndim))

    mask = data > threshold*data.max()

    sel_start = np.zeros(ndim, dtype=np.int)
    sel_end = np.zeros(ndim, dtype=np.int)

    for i in range(ndim):
        other_axes = tuple([x for x in range(ndim) if x != i])
        axis_max = mask.max(axis=other_axes)
        axis_start = np.argmax(axis_max > 0)
        axis_end = len(axis_max) - np.argmax(axis_max[::-1] > 0) - 1

        sel_start[i] = max(0, axis_start - margin)
        sel_end[i] = min(len(axis_max), axis_end + margin)

    sel_data = data[sel_start[0]:sel_end[0]]
    if ndim >= 2:
        sel_data = sel_data[:, sel_start[1]:sel_end[1]]
    if ndim == 3:
        sel_data = sel_data[:, :, sel_start[2]:sel_end[2]]

    return sel_data


def generate_report(data_stack, result_data, report_name):
    """ Create PDF report

    Keyword arguments:
    data_stack -- 4D stack of input real complex structures
    result_data -- 3D result data
    report_name -- name of output file
    """
    rep = ReportWriter(report_name)
    rep.save_figure(result_figure(result_data))
    rep.save_figure(data_stack_figure(data_stack))
    rep.close()

def result_figure(result_data):
    """ Create figure with reconstruction result

    Keyword arguments:
    result_data -- 3D result data

    Return:
    fig -- matplotlib Figure
    """

    result_data = np.real(correct_phase(result_data))
    result_data = cut_center(result_data, margin=10)
    size_z, size_y, size_x = result_data.shape

    fig, axes = plt.subplots(1,3, figsize=(12,6), dpi=200)
    plot_image(result_data[size_z//2], axes[0], logscale=False, vmax=result_data.max(),
               colorbar=False)
    plot_image(result_data[:, size_y//2], axes[1], logscale=False, vmax=result_data.max(),
               colorbar=False)
    plot_image(result_data[:, :, size_x//2], axes[2], logscale=False, vmax=result_data.max())
    return fig

def data_stack_figure(data_stack):
    """ Create figure with separate reconstruction examples

    Keyword arguments:
    data_stack -- 4D stack of input real complex structures

    Return:
    fig -- matplotlib Figure
    """

    num_images, size_z, _, _ = data_stack.shape

    plot_x = int(np.ceil(np.sqrt(num_images)))
    plot_y = int(np.ceil(num_images / plot_x))

    fig, axes = plt.subplots(plot_y, plot_x, figsize=(plot_x*3, plot_y*3), dpi=200)
    plt.subplots_adjust(left=0.05, bottom=0.05, right=0.90, top=0.85, wspace=0.2, hspace=0.2)

    for i in range(plot_x*plot_y):
        axis = axes.flatten()[i]
        if i >= num_images:
            axis.set_axis_off()
        else:
            image = np.real(correct_phase(data_stack[i])[size_z//2])
            image = cut_center(image, margin=10)
            plot_image(image, axis, logscale=False)

    return fig
