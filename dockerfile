ARG cuda_version=9.1
ARG cuda_env=devel
ARG os=centos7

FROM nvidia/cuda:${cuda_version}-${cuda_env}-${os}

RUN yum update -y && \
    yum install -y cmake make gcc gcc-c++ python3-pip python3-devel bzip2

ENV VIRTUAL_ENV=/opt/venv
RUN python3 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

RUN pip install --upgrade pip && pip install setuptools wheel --upgrade

# Install dependencies:
COPY requirements.txt .
RUN pip install -r requirements.txt

RUN curl -O http://ftp.esrf.fr/pub/scisoft/PyNX/pynx-latest.tar.bz2 && \
    tar -xjf pynx-latest.tar.bz2 && \
    cd pynx && python setup.py install && \
    cd .. && rm pynx* -rf

COPY dist/pynx_wrapper*tar.gz .
RUN pip install pynx_wrapper*

WORKDIR /work
ENV HOME="/work"
