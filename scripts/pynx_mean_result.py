#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Compute mean reconstruction from multiple results PYNX results
Author: Sergey Bobkov
"""

import os
import sys
import argparse
import numpy as np

import pynx_wrapper
from pynx_wrapper import pdfreport

def main():
    parser = argparse.ArgumentParser(description='Compute mean reconstruction from multiple results PYNX results')
    parser.add_argument('input_files', nargs='+', help='Input *.cxi or *.npz files')
    parser.add_argument('-o', '--out', required=True, help='Output file')
    parser.add_argument('--mode', default='mean_real',
                        choices=['mean_real', 'mean_recip'], help='Averaging method')
    parser.add_argument('-r', '--report', help='PDF report')
    parsed_args = parser.parse_args()

    input_files = parsed_args.input_files
    out_file = parsed_args.out
    mode = parsed_args.mode
    report_file = parsed_args.report

    for f in input_files:
        if not os.path.isfile(f):
            parser.error(f"File doesn't exist: {f}")

    if os.path.isdir(out_file):
        parser.error(f"Directory already exist: {out_file}")

    data_stack = []
    for f in input_files:
        if f.endswith('.cxi'):
            data = pynx_wrapper.read_cxi_data(f)
        elif f.endswith('.npy'):
            data = np.load(f)
        elif f.endswith('.npz'):
            with np.load(f) as npfile:
                data = npfile[npfile.files[0]]
        else:
            print(f'Unknown file format: {f}', file=sys.stderr)

        data_stack.append(data)

    data_stack = np.stack(data_stack)

    if mode == 'mean_real':
        result = pynx_wrapper.mean_real_data(data_stack)
    elif mode == 'mean_recip':
        result = pynx_wrapper.mean_recip_data(data_stack)
    else:
        parser.error(f'Unknown mode: {mode}')

    out_dir = os.path.dirname(out_file)
    if out_dir:
        os.makedirs(out_dir, exist_ok=True)

    if out_file.endswith('.cxi'):
        pynx_wrapper.save_cxi_data(out_file, result)
    elif out_file.endswith('.npy'):
        np.save(out_file, result)
    elif out_file.endswith('.npz'):
        np.savez(out_file, data=result)
    else:
        parser.error(f'Unknown file format: {f}')
    
    if report_file:
        pdfreport.generate_report(data_stack, result, report_file)


if __name__ == '__main__':
    main()
