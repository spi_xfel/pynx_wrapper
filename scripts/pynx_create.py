#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Create input file for PYNX
Author: Sergey Bobkov
"""

import os
import argparse

import pynx_wrapper

DEFAULT = pynx_wrapper.read_default_params()

def main():
    parser = argparse.ArgumentParser(description='Create input file for PYNX')
    parser.add_argument('-o', '--out', required=True)
    parser.add_argument('--data', required=True)
    parser.add_argument('--mask', default=DEFAULT['mask'])
    parser.add_argument('--detector_distance',
                        default=DEFAULT['detector_distance'], type=float)
    parser.add_argument('--input_binning', default=1, type=int)
    parser.add_argument('--pixel_size_detector',
                        default=DEFAULT['pixel_size_detector'], type=float)
    parser.add_argument('--wavelength',
                        default=DEFAULT['wavelength'], type=float)
    parser.add_argument('--verbose',
                        default=DEFAULT['verbose'], type=int)
    parser.add_argument('--auto_center_resize',
                        default=DEFAULT['auto_center_resize'], choices=['True', 'False'])
    parser.add_argument('--nb_run',
                        default=DEFAULT['nb_run'], type=int)
    parser.add_argument('--zero_mask',
                        default=DEFAULT['zero_mask'], choices=["0", "1", "auto"])
    parser.add_argument('--support_type',
                        default=DEFAULT['support_type'], choices=["circle", "square"])
    parser.add_argument('--support_size',
                        default=DEFAULT['support_size'], type=int)
    parser.add_argument('--target_size', type=int)
    parser.add_argument('--support_threshold',
                        default=DEFAULT['support_threshold'], type=float)
    parser.add_argument('--support_only_shrink',
                        default=DEFAULT['support_only_shrink'], choices=['True', 'False'])
    parser.add_argument('--support_smooth_width_begin',
                        default=DEFAULT['support_smooth_width_begin'], type=float)
    parser.add_argument('--support_smooth_width_end',
                        default=DEFAULT['support_smooth_width_end'], type=float)
    parser.add_argument('--positivity',
                        default=DEFAULT['positivity'], choices=['True', 'False'])
    parser.add_argument('--crop_output',
                        default=DEFAULT['crop_output'], type=int)
    parser.add_argument('--rebin',
                        default=DEFAULT['rebin'], type=int)
    parser.add_argument('--algorithm', default=DEFAULT['algorithm'])

    params = vars(parser.parse_args())

    if params["input_binning"] > 1:
        params["pixel_size_detector"] *= params["input_binning"]

    del params["input_binning"]

    if params["target_size"] is not None:
        supp_size = pynx_wrapper.predict_support_size(
            params["target_size"],
            params["wavelength"],
            params["pixel_size_detector"],
            params["detector_distance"],
            params["data"]
        )
        params["support_size"] = int(supp_size/2)

    del params["target_size"]

    data = params["data"]

    if not os.path.isfile(data):
        parser.error("File doesn't exist: {}".format(data))

    outfile = params["out"]
    del params["out"]

    pynx_wrapper.write_params(outfile, params)


if __name__ == '__main__':
    main()
